<?php

	namespace org\tekuna\framework\component;

	use \Exception;


	/**
	 * This Exception is thrown by various classes when noticing problems
	 * with the processing of declared components.
	 */
	class ComponentException extends Exception {

	}
