<?php
	
	namespace org\tekuna\framework\info;
	
	use org\tekuna\core\event\Event;
	
	
	/**
	 * This Event is triggered when requesting system information. The information
	 * is stored from handlers directly on this event.
	 */
	class InformationEvent implements Event {

		protected $arrInfoSections = array();
		
		
		/**
		 * Adds another information section
		 * 
		 * @param string $sTitle the section title
		 * @param string $sContent the section content (printable on a console)
		 */
		public function addInfoSection($sTitle, $sContent) {
			
			$this -> arrInfoSections[$sTitle] = $sContent;
		}
		
		
		/**
		 * @return array returns all info sections as associative array.
		 */
		public function getInfoSections() {
			
			return $this -> arrInfoSections;
		}
	}