<?php

	namespace org\tekuna\framework;

	use \Exception;
	
	use org\tekuna\base\Tekuna;
	
	use org\tekuna\core\context\Context;
	use org\tekuna\core\event\Event;
	use org\tekuna\core\event\AbstractEventListener;
	
	use org\tekuna\framework\request\Request;
	use org\tekuna\framework\request\RequestException;
	use org\tekuna\framework\request\CliRequestEvent;
	
	use org\tekuna\framework\response\ResponseEvent;
	use org\tekuna\framework\response\cli\CliOkResponse;
	use org\tekuna\framework\response\cli\CliErrorResponse;
	use org\tekuna\framework\response\cli\CliExceptionResponse;
	
	use org\tekuna\framework\context\ContextProcessor;
	use org\tekuna\framework\component\CliComponentProcessor;
	use org\tekuna\framework\action\CliActionProcessor;
	use org\tekuna\framework\action\CliActionEvent;
	

	class CliRequestListener extends AbstractEventListener {

		private $objLogger;


		public function __construct() {

			$this -> objLogger = Tekuna :: getLogger(__CLASS__);
		}

		public function handlesEvent(Event $objEvent) {
			
			return $objEvent instanceof CliRequestEvent;
		}
		
		public function handleEvent(Event $objEvent) {

			$objConP = new ContextProcessor($this -> getApplicationContext());
			
			try {

				// match component
				$objComP = new CliComponentProcessor($this -> getApplicationContext());
				$objComponentElement = $objComP -> getMatchingCliComponent();
				$this -> objLogger -> info("matched component for this request: ". trim($objComponentElement -> __toString()));

				// load the context values out of this component
				$objConP -> updateContext($objComponentElement);
				
				// match action
				$objAP = new CliActionProcessor($this -> getApplicationContext(), $objComponentElement);
				$objActionElement = $objAP -> getMatchingCliAction();
				$this -> objLogger -> info("matched action for this request: ". trim($objActionElement -> __toString()));

				// load the context values out of this action
				$objConP -> updateContext($objActionElement);
				
				// trigger the CliActionEvent
				$objActionEvent = new CliActionEvent($this -> getApplicationContext(), $objComponentElement, $objActionElement);
				$this -> getApplicationContext() -> getEventManager() -> triggerEvent($objActionEvent);
			}
			catch (RequestDispatchException $objException) {

				$objRequest = $this -> getApplicationContext() -> getRequest();
				
				$objResponse = new CliErrorResponse("No action found for the parameters '". $objRequest -> getArgument(1) ."' and '". $objRequest -> getArgument(2) ."'.");
				$objResponseEvent = new ResponseEvent($objResponse);
				$this -> getApplicationContext() -> getEventManager() -> triggerEvent($objResponseEvent);
			}
			catch (Exception $objException) {
				
				// log the exception
				Tekuna :: logException($objException);
				
				$objResponse = new CliExceptionResponse($objException);
				$objResponseEvent = new ResponseEvent($objResponse);
				$this -> getApplicationContext() -> getEventManager() -> triggerEvent($objResponseEvent);
			}
		}
	}