<?php
	
	namespace org\tekuna\framework\response;

	
	interface Response {
		
		public function sendResponse();
	}
