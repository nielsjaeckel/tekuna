<?php
	
	namespace org\tekuna\framework\response;
	
	use org\tekuna\core\event\Event;
	
	
	class ResponseEvent implements Event {

		protected $objResponse;
		
		public function __construct(Response $objResponse) {
			
			$this -> objResponse = $objResponse;
		}
		
		public function getResponse() {
			
			return $this -> objResponse;
		}
	}