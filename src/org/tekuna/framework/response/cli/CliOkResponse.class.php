<?php
	
	namespace org\tekuna\framework\response\cli;

	use org\tekuna\framework\response\cli\CliResponse;
	

	class CliOkResponse implements CliResponse {

		public function sendResponse() {

			exit(0);
		}
	}
