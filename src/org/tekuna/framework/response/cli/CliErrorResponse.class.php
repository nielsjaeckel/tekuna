<?php
	
	namespace org\tekuna\framework\response\cli;

	use org\tekuna\framework\response\ResponseException;

	use org\tekuna\framework\response\cli\CliResponse;
	

	class CliErrorResponse implements CliResponse {

		protected
			$iErrorCode,
			$sErrorMessage;
			
		public function __construct($sErrorMessage, $iErrorCode = 1) {
			
			if ($iErrorCode < 1) {
				
				throw new ResponseException("The error code must be greater than 0.");
			}
			
			if ($iErrorCode > 254) {
				
				throw new ResponseException("The error code must be smaller than 255.");
			}
			
			$this -> sErrorMessage = $sErrorMessage;
			$this -> iErrorCode = (int) $iErrorCode;
		}
		
		
		public function getErrorMessage() {
			
			return $this -> sErrorMessage;
		}
		
		
		public function getErrorCode() {
			
			return $this -> iErrorCode;
		}
		
		
		public function sendResponse() {

			// write message to STDERR
			fwrite(STDERR, $this -> sErrorMessage ."\n");
			
			// exit with error code
			exit($this -> iErrorCode);
		}
	}
