<?php
	
	namespace org\tekuna\framework\response\cli;

	use org\tekuna\framework\response\Response;
	
	
	interface CliResponse extends Response {
		
	}
