<?php
	
	namespace org\tekuna\framework\response\cli;

	use \Exception;
	
	use org\tekuna\framework\response\ResponseException;


	class CliExceptionResponse extends CliErrorResponse {

		protected $objException;
		
			
		public function __construct(Exception $objException) {
			
			$this -> objException = $objException;
		
			// echo exception without stack trace
			$sMessage = "\n\n";
			$sMessage .= "========================================================\n";
			$sMessage .= "=== Application aborted due to an uncaught Exception ===\n";
			$sMessage .= "========================================================\n";

			// print all causes
			$objCurrentException = $objException;

			do {

				if ($objCurrentException != $objException) {

					$sMessage .= "\ncaused by:\n";
				}

				$sMessage .= "\n== ". get_class($objCurrentException) ." ==\n";
				$sMessage .= $objCurrentException -> getMessage() ."\n";
				$objCurrentException = $objCurrentException -> getPrevious();

			} while ($objCurrentException != NULL);
		
			// call parent constructor
			parent :: __construct($sMessage);
		}
		
		public function getException() {
			
			return $this -> objException;
		}
	}
