<?php
	
	namespace org\tekuna\framework\response\http;
	
	use org\tekuna\framework\request\Url;


	class UrlRedirectResponse extends RedirectResponse {

		public function __construct(Url $objUrl, $bPermanent = false) {
			
			// call parent constructor
			parent :: __construct($objUrl -> renderAbsolute(), $bPermanent);
		}
	}
