<?php
	
	namespace org\tekuna\framework\response\http;

	use org\tekuna\core\context\Context;

	use org\tekuna\framework\response\ResponseException;
	

	class FileResponse extends AbstractHttpResponse {
		
		protected
			$sFilename,
			$sMimeType,
			$sContentDisposition,
			$sFilenameForDownload;

		
		public function __construct($sFilename, $sMimeType = 'application/octet-stream', $sContentDisposition = 'inline', $sFilenameForDownload = NULL) {
			
			if (! is_file($sFilename)) {
				
				throw new ResponseException("The file '$sFilename' does not exist.");
			}
			
			if (! is_readable($sFilename)) {
				
				throw new ResponseException("The file '$sFilename' is not readable.");
			}
			
			if ($sFilenameForDownload === NULL) {
				
				$sFilenameForDownload = basename($sFilename);
			}
			
			$this -> sFilename = $sFilename;
			$this -> sMimeType = $sMimeType;
			$this -> sContentDisposition = $sContentDisposition;
			$this -> sFilenameForDownload = $sFilenameForDownload;
		}

		public function sendHeaders() {

			// send Tekuna 'marker'
			header('X-Powered-By: ##APPLICATION_NAME## ##TEKUNA_VERSION##');
			
			// send file headers
			header("Content-Length: ". filesize($this -> sFilename));
			header("Content-Disposition: $this->sContentDisposition;filename=$this->sFilenameForDownload");

			// TODO: support more Content-Disposition parameters
			// see http://www.faqs.org/rfcs/rfc2183.html
			
			// TODO: check for cross-browser compatibility
			// see http://de2.php.net/manual/en/function.header.php
		}
		
		public function sendContent() {
			
			// send file directly to output
			readfile($this -> sFilename);
		}
	}
