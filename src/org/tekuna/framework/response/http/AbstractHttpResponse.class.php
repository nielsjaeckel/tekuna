<?php
	
	namespace org\tekuna\framework\response\http;

	use org\tekuna\core\context\Context;

	
	/**
	 * 
	 * 
	 * Hint:
	 * No Cookie or Session abstraction. To be done as usual 
	 * through setcookie(...) and $_SESSION array.
	 * 
	 */
	abstract class AbstractHttpResponse implements HttpResponse {
		
		protected
			$sProtocol = 'HTTP/1.1',
			$iStatus = 200,
		
			$arrHttpStatusMessages = array(100 => 'Continue',
			                               101 => 'Switching Protocols',
			                               200 => 'OK',
			                               201 => 'Created',
			                               202 => 'Accepted',
			                               203 => 'Non-Authoritative Information',
			                               204 => 'No Content',
			                               205 => 'Reset Content',
			                               206 => 'Partial Content',
			                               300 => 'Multiple Choices',
			                               301 => 'Moved Permanently',
			                               302 => 'Moved Temporarily',
			                               303 => 'See Other',
			                               304 => 'Not Modified',
			                               304 => 'Use Proxy',
			                               307 => 'Temporary Redirect',
			                               400 => 'Bad Request',
			                               401 => 'Unauthorized',
			                               402 => 'Payment Required',
			                               403 => 'Forbidden',
			                               404 => 'Not Found',
			                               405 => 'Method Not Allowed',
			                               406 => 'Not Acceptable',
			                               407 => 'Proxy Authentication Required',
			                               408 => 'Request Timeout',
			                               409 => 'Conflict',
			                               410 => 'Gone',
			                               411 => 'Length Required',
			                               412 => 'Precondition Failed',
			                               413 => 'Request Entity Too Large',
			                               414 => 'Request-URL Too Long',
			                               415 => 'Unsupported Media Type',
			                               416 => 'Requested Range Not Satisfiable',
			                               417 => 'Expectation Failed',
			                               500 => 'Internal Server Error',
			                               501 => 'Not Implemented',
			                               502 => 'Bad Gateway',
			                               503 => 'Service Unavailable',
			                               504 => 'Gateway Timeout',
			                               505 => 'HTTP Version Not Supported');
		
		public function sendStatus() {

			// TODO: send different header for FastCGI 
			// (see http://de2.php.net/manual/en/function.header.php)
			$sMessage = $this -> getStatusMessage($this -> iStatus);
			header("$this->sProtocol $this->iStatus $sMessage");
		}

		
		/**
		 * Send the HTTP response to the client. Calls the HttpResponse
		 * interface methods in this order:
		 * 
		 *   - sendStatus()
		 *   - sendHeaders()
		 *   - sendContent()
		 * 
		 * Note:
		 * This method is called automatically by the ResponseListener,
		 * in most cases you do not need to call it for yourself.
		 */
		public function sendResponse() {

			// send all response parts
			$this -> sendStatus();
			$this -> sendHeaders();
			$this -> sendContent();
		}
		
		public function setProtocol($sProtocol) {
			
			$this -> sProtocol = $sProtocol;
		}
		
		public function getProtocol() {
			
			return $this -> sProtocol;
		}
		
		public function setStatus($iStatus) {
			
			if (! isset($this -> arrHttpStatusMessages[$iStatus])) {
				
				throw new InvalidArgumentException("The status code '$iStatus' is not valid for an HTTP Response.");
			}
			
			$this -> iStatus = (int) $iStatus;
		}
		
		public function getStatus() {
			
			return $this -> iStatus;
		}
		
		public function getStatusMessage($iStatus) {
			
			return $this -> arrHttpStatusMessages[$iStatus];
		}
	}
