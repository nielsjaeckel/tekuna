<?php
	
	namespace org\tekuna\framework\response\http;

	use \Exception;
	
	use org\tekuna\core\context\Context;
	
	use org\tekuna\framework\request\HttpRequest;
	
	
	class FiveOhOhResponse extends ContentResponse {
		
		public function __construct(HttpRequest $objRequest, Exception $objException) {

			$sMessage = '<h1>Error 500: Internal Server Error</h1>';
			$sMessage .= '<p>An Exception occured during processing the request "'. $objRequest -> getRequestAction() .'":</p>';
			
			// print all causes
			$objCurrentException = $objException;

			do {

				if ($objCurrentException != $objException) {

					$sMessage .= '<p>caused by:</p>';
				}

				$sMessage .= '<h2>'. get_class($objCurrentException) .'</h2>';
				$sMessage .= '<p>'. $objCurrentException -> getMessage() .'</p>';
				$objCurrentException = $objCurrentException -> getPrevious();

			} while ($objCurrentException != NULL);
		
			// call parent constructor with error code 500.
			parent :: __construct($sMessage, 500);
			
			// do not compress the output
			$this -> setCompressOutput(false);
		}
	}
