<?php
	
	namespace org\tekuna\framework\response\http;

	use org\tekuna\core\context\Context;

	use org\tekuna\framework\response\ResponseException;
	

	class FileDownloadResponse extends FileResponse {
		
		public function __construct($sFilename, $sMimeType = 'application/octet-stream', $sContentDisposition = 'inline', $sFilenameForDownload = NULL) {
			
			parent :: __construct($sFilename, $sMimeType, 'Attachment', $sFilenameForDownload);
		}
	}
