<?php
	
	namespace org\tekuna\framework\response\http;

	use org\tekuna\core\context\Context;
	
	use org\tekuna\framework\request\HttpRequest;
	
	
	class FourOhFourResponse extends ContentResponse {
		
		public function __construct(HttpRequest $objRequest) {
			
			$sMessage = '<h1>Error 404: Not Found</h1>';
			$sMessage .= '<p>The request for "'. $objRequest -> getRequestAction() .'" could not be processed.</p>';
			
			parent :: __construct($sMessage, 404);
			
			// do not compress the output
			$this -> setCompressOutput(false);
		}
	}
