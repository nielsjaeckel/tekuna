<?php
	
	namespace org\tekuna\framework\response\http;

	use org\tekuna\framework\response\Response;
	
	
	interface HttpResponse extends Response {
		
		public function sendStatus();
		
		public function sendHeaders();
		
		public function sendContent();
	}
