<?php

	namespace org\tekuna\framework\action;

	use org\tekuna\core\configuration\ConfigurationElement;
	use org\tekuna\core\context\Context;

	/**
	 * Abstract implementation of the Action interface that provides
	 * most of the methods that have to be implemented.
	 */
	abstract class AbstractAction implements Action {
		
		private
			$objContext = NULL,
			$objConfigurationElement = NULL;
			
		/**
		 * (non-PHPdoc)
		 * @see org\tekuna\core\context\ApplicationContextAware::setApplicationContext()
		 */
		public function setApplicationContext(Context $objContext) {
			
			$this -> objContext = $objContext;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see org\tekuna\core\context\ApplicationContextAware::getApplicationContext()
		 */
		public function getApplicationContext() {
			
			return $this -> objContext;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see org\tekuna\core\configuration\Configured::setConfigurationElement()
		 */
		public function setConfigurationElement(ConfigurationElement $objElement) {
			
			$this -> objConfigurationElement = $objElement;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see org\tekuna\core\configuration\Configured::getConfigurationElement()
		 */
		public function getConfigurationElement() {
			
			return $this -> objConfigurationElement;
		}
	}
