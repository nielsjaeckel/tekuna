<?php

	namespace org\tekuna\framework\action;

	use \Exception;

	/**
	 * Exception for Actions
	 * This exception is thrown by various classes when processing actions.
	 */
	class ActionException extends Exception {

	}
