<?php
	
	namespace org\tekuna\framework\action;
	
	use org\tekuna\core\event\Event;
	
	
	/**
	 * This ActionEvent is triggered when an Action is matched within a
	 * CLI request.
	 */
	class CliActionEvent extends ActionEvent {

		/**
		 * Provides information for the ApplicationInfoListener.
		 */
		public function handleEvent(Event $objEvent) {
			
			$sOutput = '';
			$sOutput .= 'Action Event: '. get_class($this) ."\n";
			$sOutput .= 'Action Class: '. get_class($this -> getAction()) ."\n";
			
			// add new info section
			$objEvent -> addInfoSection('Cli Action Information', $sOutput);
		}
	}