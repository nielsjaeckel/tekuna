<?php

	namespace org\tekuna\framework\action;
	
	use org\tekuna\core\configuration\Configured;
	use org\tekuna\core\context\ApplicationContextAware;
	use org\tekuna\core\context\ContextObjectSupplied;

	use org\tekuna\framework\request\Request;
	
	
	/**
	 * Interface that every action must implement. Use the AbstractAction
	 * when implementing your concrete actions - this abstract class
	 * provides most of the necessary methods.
	 */
	interface Action extends ApplicationContextAware, ContextObjectSupplied, Configured {

		/**
		 * If this action is triggered within a HttrRequestEvent,
		 * this method must return an instance of HttpResponse.
		 * 
		 * @param ActionEvent $objActionEvent
		 * @param Request $objRequest
		 */
		public function execute(ActionEvent $objActionEvent, Request $objRequest);
	}