<?php

	namespace org\tekuna\framework\interceptor;

	use org\tekuna\core\configuration\Configured;
	use org\tekuna\core\context\ApplicationContextAware;
	use org\tekuna\core\context\ContextObjectSupplied;
	use org\tekuna\core\context\Context;

	
	/**
	 * The Interceptor interface is used for all elements of an invocation
	 * chain, except the intercepted action.
	 */
	interface Interceptor extends ApplicationContextAware, ContextObjectSupplied, Configured {

		/**
		 * This is the actual intercepting method. The given instance of the
		 * InvocationChain should be used to continue the the program flow.
		 * You can also decide not to call $objChain -> invokeNext(); but then
		 * you have to return a valid Response object. That object is otherwise
		 * returned from inner Interceptors or the action
		 * 
		 * @param InvocationChain $objChain the current chain of invocation with all
		 *        interceptors and (the innermost) the action.
		 *        
		 * @return Response the response to send to the client. Responses of
		 *         inner interceptors or the action are returned from the call to
		 *         invokeNext() on the chain.
		 */
		public function intercept(InvocationChain $objChain);
	}