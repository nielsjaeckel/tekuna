<?php

	namespace org\tekuna\framework\interceptor;

	use org\tekuna\core\configuration\ConfigurationElement;
	use org\tekuna\core\context\Context;

	
	/**
	 * Abstract implementation of the Interceptor interface that provides
	 * most of the needed methods
	 */
	abstract class AbstractInterceptor implements Interceptor {
		
		private
			$objContext = NULL,
			$objConfigurationElement = NULL;
			
		
		/**
		 * (non-PHPdoc)
		 * @see src/org/tekuna/core/context/org\tekuna\core\context.ApplicationContextAware::setApplicationContext()
		 */
		public function setApplicationContext(Context $objContext) {
			
			$this -> objContext = $objContext;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see src/org/tekuna/core/context/org\tekuna\core\context.ApplicationContextAware::getApplicationContext()
		 */
		public function getApplicationContext() {
			
			return $this -> objContext;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see src/org/tekuna/core/configuration/org\tekuna\core\configuration.Configured::setConfigurationElement()
		 */
		public function setConfigurationElement(ConfigurationElement $objElement) {
			
			$this -> objConfigurationElement = $objElement;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see src/org/tekuna/core/configuration/org\tekuna\core\configuration.Configured::getConfigurationElement()
		 */
		public function getConfigurationElement() {
			
			return $this -> objConfigurationElement;
		}
	}
