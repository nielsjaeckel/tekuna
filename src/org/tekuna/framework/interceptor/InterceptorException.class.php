<?php

	namespace org\tekuna\framework\interceptor;

	use \Exception;


	/**
	 * This Exception is thrown if anything goes wrong with the interception.
	 */
	class InterceptorException extends Exception {

	}
