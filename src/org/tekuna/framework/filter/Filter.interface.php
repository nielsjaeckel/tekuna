<?php

	namespace org\tekuna\framework\filter;

	/**
	 * Interface for filters that are used by the Tekuna Request object
	 * to filter input values from various scopes (GET, POST)
	 */
	interface Filter {


		/**
		 * The filter method accepts a mixed type input and returns some
		 * filtered data (usually as string)
		 *
		 * @param mixed $mInput the filter input
		 * @return mixed the filtered output
		 */

		public function filter($mInput);
	}
