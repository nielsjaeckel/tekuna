<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter can be used to sanitize inputs that will be part
	 * of SQL statements. It is a combination of addslashes and some
	 * control character replacements.
	 */
	class SqlFilter implements Filter {


		/**
		 * Apply the filter
		 *
		 * @param mixed $mInput
		 * @return string
		 */
		public function filter($mInput) {

			// standard escapes
			$mInput = addslashes($mInput);

			// special escapes
			$mInput = str_replace("\n", "\\\n", $mInput);
			$mInput = str_replace("\r", "\\\r", $mInput);
			$mInput = str_replace("\t", "\\\t", $mInput);
			$mInput = str_replace("\x1a", "\\\x1a", $mInput);

			return $mInput;
		}
	}
