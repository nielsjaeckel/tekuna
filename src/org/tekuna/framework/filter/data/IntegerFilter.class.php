<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;

	/**
	 * Converts incoming data into integer values.
	 */
	class IntegerFilter implements Filter {


		/**
		 * The filter method applies only the intval() function.
		 *
		 * @param mixed $mInput the raw input
		 * @return int the "found" integer
		 */
		public function filter($mInput) {

			return intval($mInput);
		}
	}
