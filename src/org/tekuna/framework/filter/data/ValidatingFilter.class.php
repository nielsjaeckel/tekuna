<?php

	namespace org\tekuna\framework\filter\data;

	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FilterException;
	

	class ValidatingFilter implements Filter {

		protected $objInnerFilter;


		public function __construct(Filter $objInnerFilter) {
			
			$this -> objInnerFilter = $objInnerFilter;
		}


		/**
		 * Runs the inner filter and checks if the data was changed
		 * by that filter. If the data was changed, a FilterException 
		 * is thrown.
		 *
		 * @param mixed $mInput the unfiltered input
		 * @throws FilterException if inner filter changes the value
		 * @return mixed the multiple filtered output
		 */
		public function filter($mInput) {

			// apply the inner filter
			$mOutput = $this -> objInnerFilter -> filter($mInput);
			
			// check if the data was changed
			if ($mOutput !== $mInput) {
				
				$sFilterClass = get_class($this -> objInnerFilter);
				throw new FilterException("The data was changed by the inner filter '$sFilterClass'.");
			}
			
			// return the output
			return $mOutput;
		}
	}
