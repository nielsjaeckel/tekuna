<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter limits input strings to a given maximum length. This
	 * can be used for such fields that allow a maximum characters count,
	 * e.g. postal code or length-delimited comments
	 */
	class LengthFilter implements Filter {

		protected $iMaxLenght = 0;


		/**
		 * Build a new LengthFilter with a certain maximum length
		 *
		 * @param int $iMaxLength the maximum character count
		 */
		public function __construct($iMaxLength) {

			$this -> iMaxLength = $iMaxLength;
		}


		/**
		 * Apply the filter
		 *
		 * @param mixed $mInput
		 * @return string
		 */
		public function filter($mInput) {

			// cut out only the first characters
			return substr($mInput, 0, $this -> iMaxLength);
		}
	}
