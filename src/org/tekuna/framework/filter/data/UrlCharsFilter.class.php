<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter removes all those characters that may not appear
	 * within a valid URL. The following characters are considered as
	 * valid URL characters:
	 *
	 *   - a-z
	 *   - A-Z
	 *   - 0-9
	 *   - ,
	 *   - .
	 *   - ;
	 *   - !
	 *   - -
	 *   - _
	 *   - /
	 *   - #
	 *   - ?
	 *   - &
	 *   - %
	 *   - =
	 *   - @
	 */
	class UrlCharsFilter implements Filter {


		/**
		 * Removes all characters that are no valid URL characters
		 *
		 * @param mixex $mInput
		 * @return string
		 */
		public function filter($mInput) {

			return preg_replace('~[^a-zA-Z0-9,\.:;!\-_/#\?\&%=@]~', '', $mInput);
		}
	}
