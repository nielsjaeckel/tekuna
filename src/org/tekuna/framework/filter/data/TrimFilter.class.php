<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter removes leading and trailing spaces of given strings.
	 */
	class TrimFilter implements Filter {


		/**
		 * Remove leading and trailing spaces. The result is of type string.
		 *
		 * @param mixed $mInput the raw string value
		 * @return string the trimmed string
		 */
		public function filter($mInput) {

			return trim((string) $mInput);
		}
	}
