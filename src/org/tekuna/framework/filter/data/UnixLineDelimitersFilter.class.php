<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * Converts all file endings in the given string to the Unix style (LF only).
	 * The Windows style (CRLF) and the old Mac OS style (CR) are handled.
	 */
	class UnixLineDelimitersFilter implements Filter {


		/**
		 * The filter method converts the line endings
		 */
		public function filter($mInput) {

			// change the line delimiters
			$mInput = str_replace("\r\n", "\n", $mInput);
			$mInput = str_replace("\r", "\n", $mInput);

			// return the modified file
			return $mInput;
		}
	}
