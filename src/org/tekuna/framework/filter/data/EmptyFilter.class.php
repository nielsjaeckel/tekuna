<?php


	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * The EmptyFilter does nothing. It is a filter that makes it possible
	 * to work with the unfiltered input values directly.
	 */
	class EmptyFilter implements Filter {


		/**
		 * The filter method just returns its input.
		 *
		 * @param mixed $mInput
		 * @return mixed
		 */
		public function filter($mInput) {

			return $mInput;
		}
	}
