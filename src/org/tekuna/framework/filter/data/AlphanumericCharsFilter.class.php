<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;

	/**
	 * This filter accepts only alphanumeric filters. These
	 * are a-z, A-Z and 0-9. Spaces are not allowed, too.
	 */
	class AlphanumericCharsFilter implements Filter {


		/**
		 * Replaces all characters except the alphanumeric ones by the empty string
		 *
		 * @param mixed $mInput
		 * @return string no more special characters
		 */
		public function filter($mInput) {

			return preg_replace('~[^a-zA-Z0-9]~', '', $mInput);
		}
	}
