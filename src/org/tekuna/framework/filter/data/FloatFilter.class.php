<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter converts incoming values to float values. Both
	 * characters , and . are considered as a comma (german notation).
	 */
	class FloatFilter implements Filter {


		/**
		 * Transform the input value into a float value. Any occurence of
		 * a comma (,) will be transformed to a point (.) By doing this, the
		 * german float number notation with comma is supported implicitly.
		 *
		 * @param mixed $mInput the raw float value
		 * @return float a real float
		 */
		public function filter($mInput) {

			// replace german commas by points
			$mInput = str_replace(',', '.', $mInput);

			return floatval($mInput);
		}
	}
