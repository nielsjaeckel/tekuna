<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter replaces HTML and XML entities in the input stream with
	 * their original characters. XML entities can be denoted as decimal
	 * or hexadecimal way.
	 */
	class EntityDecodeFilter implements Filter {


		/**
		 * Performs the filtering
		 *
		 * @param mixed $mInput the input with entities
		 * @return string all known entities back-substituted
		 */
		public function filter($mInput) {

			// decode XML entities
			$mInput = preg_replace('~&#x([0-9a-f]{1,7});~ei', 'chr(hexdec("\\1"))', $mInput);
			$mInput = preg_replace('~&#([0-9]{1,7});~e', 'chr(\\1)', $mInput);

			// decode HTML entities
			$arrTable = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
			$arrTable = array_flip($arrTable);
			$mInput = strtr($mInput, $arrTable);

			return $mInput;
		}
	}
