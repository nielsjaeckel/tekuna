<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter implementation escapes all special XML characters
	 * with their octal entity notation. The following characters are
	 * changed: &, <, >, ", '
	 */
	class XmlSpecialCharsFilter implements Filter {


		/**
		 * Transforms the input
		 *
		 * @param mixed $mInput
		 * @return string
		 */
		public function filter($mInput) {

			$mInput = str_replace('&', '&#38;', $mInput);
			$mInput = str_replace('<', '&#60;', $mInput);
			$mInput = str_replace('>', '&#62;', $mInput);
			$mInput = str_replace('"', '&#34;', $mInput);
			$mInput = str_replace('\'', '&#39;', $mInput);

			return $mInput;
		}
	}
