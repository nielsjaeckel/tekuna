<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter removes all well-formed HTML from the input.
	 */
	class NoHtmlFilter implements Filter {


		/**
		 * The filter method is just a wrapper for the strip_tags()
		 * function of PHP.
		 *
		 * @param mixed $mInput the input
		 * @return string the input without the tags
		 */
		public function filter($mInput) {

			// remove any disallowed characters and attribute injection
			// taken from the book php|architect's Guide to PHP Security page 62
			$mInput = preg_replace('~<([A-Z]\w*)(?:\s* (?:\w+) \s* = \s* (?(?=["\']) (["\'])(?:.*?\2)+ | (?:[^\s>]*) ) )* \s* (\s/)? >~ix', '<\1\5>', $mInput);

			// remove all well-formed tags
			$mInput = strip_tags($mInput);

			return $mInput;
		}
	}
