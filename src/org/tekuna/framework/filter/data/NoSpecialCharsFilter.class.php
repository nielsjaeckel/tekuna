<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter removes all special character from the input.
	 * As normal characters the following are considered:
	 *
	 *   - a-z
	 *   - A-Z
	 *   - 0-9
	 *   - ,
	 *   - .
	 *   - :
	 *   - ;
	 *   - !
	 *   - -
	 *   - _
	 *
	 * This filter could be used as a sanitizer for simple texts.
	 *
	 * NOTE: German umlauts are filtered out.
	 */
	class NoSpecialCharsFilter implements Filter {


		/**
		 * Replaces all special characters by the empty string
		 *
		 * @param mixed $mInput
		 * @return string no more special characters
		 */
		public function filter($mInput) {

			return preg_replace('~[^a-zA-Z0-9,\.;:!\-_ ]~', '', $mInput);
		}
	}
