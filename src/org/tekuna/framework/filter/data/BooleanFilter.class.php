<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter converts incoming data into a boolean value.
	 */
	class BooleanFilter implements Filter {


		/**
		 * The input is casted to boolean. Additionally it is checked
		 * to be the string 'false' that is considered as the boolean
		 * false value, too.
		 *
		 * @param mixed $mInput the input
		 * @return boolean
		 */
		public function filter($mInput) {

			// TODO: throw exception here; like confignode;

 			// additionally to PHP's boolean typecasts, consider
 			// the string 'false' as the boolean false value
 			if (strtolower(trim($mInput)) === 'false') {

 				return false;
 			}

			return (boolean) $mInput;
		}
	}
