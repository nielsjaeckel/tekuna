<?php

	namespace org\tekuna\framework\filter\data;

	use org\tekuna\framework\filter\data\UrlCharsFilter;

	/**
	 * This filter extends the URLCharsFilter by ensuring a beginning with a slash.
	 */
	class FrameworkActionFilter extends UrlCharsFilter {


		/**
		 * Removes all characters that are no valid URL characters
		 * and prepends a slash if this is not still there.
		 *
		 * @param mixex $mInput
		 * @return string
		 */
		public function filter($mInput) {

			$mInput = parent :: filter($mInput);

			// ensure minimal length
			if (strlen($mInput) == 0) {

				$mInput = '/';
			}

			// prepend slash if not still there
			if ($mInput{0} != '/') {

				$mInput = '/'. $mInput;
			}

			return $mInput;
		}
	}
