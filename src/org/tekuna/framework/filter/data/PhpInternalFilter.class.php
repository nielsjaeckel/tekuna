<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter is an adapter class to use PHP's filter facilities
	 * with Tekuna. Example:
	 *
	 * PHP:
	 *      filter_var($sInput, FILTER_SANITIZE_STRIPPED);
	 *
	 * Tekuna:
	 *      new PhpInternalFilter(FILTER_SANITIZE_STRIPPED);
	 */
	class PhpInternalFilter implements Filter {

		protected
			$iFilterType = 0,
			$mOptions = NULL;


		/**
		 * Construct a new filter adapter for a given filter type
		 *
		 * @param int $iFilterType the int-ID of the filter type (usually as constant)
		 * @param mixed $mOptions optional; if the used filter requires additionnaly options
		 */
		public function __construct($iFilterType, $mOptions = NULL) {

			$this -> iFilterType = $iFilterType;
			$this -> mOptions = $mOptions;
		}


		/**
		 * Applies the filter with the given input with the filter_var() function.
		 *
		 * @param mixed $mInput
		 * @return mixed the output
		 */
		public function filter($mInput) {

			if ($this -> mOptions === NULL) {

				return filter_var($mInput, $this -> iFilterType);
			}
			else {

				return filter_var($mInput, $this -> iFilterType, $this -> mOptions);
			}
		}
	}
