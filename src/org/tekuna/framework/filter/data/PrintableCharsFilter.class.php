<?php

	namespace org\tekuna\framework\filter\data;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;


	/**
	 * This filter removes all control characters, except new lines and tabs
	 */
	class PrintableCharsFilter implements Filter {


		/**
		 * Filter out all control characters
		 *
		 * @param mixed $mInput
		 * @return string
		 */
		public function filter($mInput) {

			// replace all control characters, except new lines (#10) and tabs (#9)
			return preg_replace('~[\x00-\x08\x0B-\x1F]~', '', $mInput);
		}
	}
