<?php

	namespace org\tekuna\framework\filter;


	use org\tekuna\framework\request\PostFile;

	/**
	 * Interface for filters that are used to filter uploaded files
	 * by the Request object.
	 *
	 * Instances of this interface could be used to change the object type
	 * of uploaded files. The given PostFile object could be transformed
	 * to a custom PostFile subclass, that has additional file handling
	 * facilities or so.
	 */
	interface FileFilter {


		/**
		 * The filter method accepts an object of type PostFile and returns
		 * such an object or NULL.
		 *
		 * @param PostFile $objFile the unfiltered file representation object
		 * @return PostFile the filtered object
		 */
		public function filter(PostFile $objFile);
	}
