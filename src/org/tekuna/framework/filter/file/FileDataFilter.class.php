<?php

	namespace org\tekuna\framework\filter\file;


	use org\tekuna\framework\filter\Filter;
	use org\tekuna\framework\filter\FileFilter;
	use org\tekuna\framework\request\PostFile;


	/**
	 * The FileDataFilter is an adapter filter, that applies normal TekunaFilters to
	 * the content of uploaded files. The constructor requires an object of type
	 * TekunaFilter. The filter method of this filter is used to modify the file
	 * content in the filter method.
	 */
	class FileDataFilter implements FileFilter {

		protected $objDataFilter = NULL;


		/**
		 * The construction of a FileDataFilter requires a TekunaFilter
		 * to be applied to the file's data
		 *
		 * @param TekunaFilter $objDataFilter the filter for the file contents
		 */
		public function __construct(Filter $objDataFilter) {

			$this -> objDataFilter = $objDataFilter;
		}


		/**
		 * Loads the content of the temp file name of the uploaded file, use the
		 * data filter to modify this content and write it back to the temp file.
		 *
		 * @param PostFile $objFile
		 * @return PostFile
		 */
		public function filter(PostFile $objFile) {

			// get the temp file contents
			$sContents = file_get_contents($objFile -> getTempName());

			// apply the data filter to the file contents
			$sContents = $this -> objDataFilter -> filter($sContents);

			// set the temp file contents
			file_put_contents($objFile -> getTempName(), $sContents);

			// return the modified file
			return $objFile;
		}
	}
