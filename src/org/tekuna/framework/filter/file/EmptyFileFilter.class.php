<?php

	namespace org\tekuna\framework\filter\file;


	use org\tekuna\framework\filter\FileFilter;
	use org\tekuna\framework\request\PostFile;



	/**
	 * This is an empty file filter that doesn't apply any kind of filter
	 * to the uploaded file. The given PostFile object is returned as it is.
	 *
	 * This filter is used if you want to work with the raw uploaded file
	 */
	class EmptyFileFilter implements FileFilter {


		/**
		 * Empty filter method that just returns the given PostFile instance
		 *
		 * @param PostFile $objFile
		 * @return PostFile
		 */
		public function filter(PostFile $objFile) {

			return $objFile;
		}
	}
