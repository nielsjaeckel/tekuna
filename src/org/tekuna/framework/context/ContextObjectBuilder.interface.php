<?php

	namespace org\tekuna\framework\context;

	use org\tekuna\core\context\Context;
	use org\tekuna\core\configuration\ConfigurationElement;

	
	/**
	 * Interface that defines a method a class must implement when it is used
	 * in the configuration to build a context object.
	 */
	interface ContextObjectBuilder {

		/**
		 * Build the new context object
		 * 
		 * @param Context $objContext the target context
		 * @param ConfigurationElement $objContextConfigElement the defining configuration element
		 */
		public static function buildContextObject(Context $objContext, ConfigurationElement $objContextConfigElement);

	}
