<?php

	namespace org\tekuna\framework\request;


	/**
	 * The PostFile class abstracts uploaded files and provides an easy access
	 * to the values of the file.
	 */
	class PostFile {

		protected
			$sID,
			$arrData;


		/**
		 * The constructor of the PostFile class is used by the Request class
		 * when importing uploaded file data. If there were any upload errors
		 * a RequestException is thrown.
		 *
		 * @param string $sID the identifier of the file
		 * @param array $arrData the file data
		 */

	    public function __construct($sID, array $arrData) {

	    	$this -> sID = $sID;
	    	$this -> arrData = $arrData;

	    	switch ($this -> arrData['error']) {

				// file bigger than allowed from the php.ini
	    		case UPLOAD_ERR_INI_SIZE :
	    				throw new RequestException('The uploaded file exceeds the maximum filesize defined in the php.ini.');
	    				break;

				// file bigger than allowed from the HTML form
	    		case UPLOAD_ERR_FORM_SIZE:
	    				throw new RequestException('The uploaded file exceeds the maximum filesize defined in the HTML form.');
	    				break;

				// file submitted only partial
	    		case UPLOAD_ERR_PARTIAL :
	    				throw new RequestException('The uploaded file was transmitted only partial. Please try again.');
	    				break;

				// no temp dir for uploaded files found
	    		case UPLOAD_ERR_NO_TMP_DIR:
	    				throw new RequestException('The uploaded file could not be stored. Temporary directory missing.');
	    				break;

				// write error for the uploaded file
	    		case UPLOAD_ERR_CANT_WRITE:
	    				throw new RequestException('The uploaded file could not be stored. Could not write into temporary directory.');
	    				break;
	    	}
	    }


		/**
		 * @return string returns the identifier of the file
		 */

	    public function getID() {

	    	return $this -> sID;
	    }


		/**
		 * @return string returns the original file name
		 */

	    public function getFileName() {

	    	return basename($this -> arrData['name']);
	    }


		/**
		 * @return string returns the temporal file name of the uploaded
		 *                file on the file system of the server
		 */

	    public function getTempName() {

	    	return $this -> arrData['tmp_name'];
	    }


		/**
		 * @return integer returns the file size in bytes
		 */

	    public function getFileSize() {

	    	return intval($this -> arrData['size']);
	    }


		/**
		 * @return string returns the submitted mime type of the file
		 */

	    public function getMimeType() {

	    	return $this -> arrData['type'];
	    }


		/**
		 * @return integer returns the error code. See PHP manual for all possible values.
		 */

	    public function getErrorCode() {

	    	return $this -> arrData['error'];
	    }


		/**
		 * Move the uploaded file to a new destination file.
		 *
		 * @param string $sDestination the new file name
		 * @param boolean $bForce optional; overwrite the file if it exists
		 */

	    public function moveTo($sDestination, $bForce = FALSE) {

	    	// check if file exists
	    	$bExists = file_exists($sDestination);

	    	// return NULL if file exists and no override force
	    	if ($bExists && ! $bForce) {

	    		return NULL;
	    	}
	    	else {

	    		return move_uploaded_file($this -> getTempName(), $sDestination);
	    	}
	    }
	}
