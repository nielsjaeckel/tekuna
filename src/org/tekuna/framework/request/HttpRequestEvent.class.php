<?php
	
	namespace org\tekuna\framework\request;
	
	use org\tekuna\core\event\Event;
	
	
	/**
	 * This event is triggered when the HTTP Request is loaded 
	 * and can be handled.
	 */
	class HttpRequestEvent extends RequestEvent {

	}