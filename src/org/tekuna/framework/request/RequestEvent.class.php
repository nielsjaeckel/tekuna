<?php
	
	namespace org\tekuna\framework\request;
	
	use org\tekuna\core\event\Event;
	
	
	/**
	 * This event is triggered when the Request is loaded and can be
	 * handled.
	 */
	class RequestEvent implements Event {
	
		protected $objRequest = NULL;
		
		public function __construct(Request $objRequest) {
			
			$this -> objRequest = $objRequest;
		}
		
		public function getRequest() {
			
			return $this -> objRequest;
		}
	}