<?php

	namespace org\tekuna\core\context;

	
	/**
	 * This is a marker interface for objects that get initialized
	 * ONCE after an autowiring
	 */
	interface ContextInitialized {

		public function initialize();
	}
