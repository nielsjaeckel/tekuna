<?php

	namespace org\tekuna\core\context;

	
	/**
	 * This is a marker interface for objects that may get context object
	 * supplied via special named setter methods.
	 * 
	 * Example:
	 * when an implementing action has a method
	 *    setEventManager()
	 * the Context will call this method with the argument
	 * named EventManager from the application context.
	 */
	interface ContextObjectSupplied {

	}
