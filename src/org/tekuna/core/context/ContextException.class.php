<?php

	namespace org\tekuna\core\context;

	use \Exception;


	/**
	 * This exception is thrown by the 
	 * context in several occasions.
	 */
	class ContextException extends Exception {
	
	}
