<?php

	namespace org\tekuna\core\context;


	/**
	 * Interface for objects that must hold a reference to the application context.
	 */
	interface ApplicationContextAware {
		
		/**
		 * Injection method for the application context
		 * 
		 * @param Context $objContext the application context
		 */
		public function setApplicationContext(Context $objContext);
				
		
		/**
		 * @return Context returns the application context
		 */
		public function getApplicationContext();
	}