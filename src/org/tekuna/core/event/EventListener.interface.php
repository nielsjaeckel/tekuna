<?php
	
	namespace org\tekuna\core\event;
	
	use org\tekuna\core\configuration\Configured;
	use org\tekuna\core\context\ApplicationContextAware;
	use org\tekuna\core\context\ContextObjectSupplied;
	use org\tekuna\core\context\Context;
	
	
	/**
	 * Interface for an event listener registered with the EventManager.
	 */
	interface EventListener extends ApplicationContextAware, ContextObjectSupplied, Configured {
	
		/**
		 * This method is called by the EventManager to decide whether
		 * to call handleEvent() of this listener for a certain event
		 * or not. Usually implemented with an instanceof check.
		 * 
		 * Return true if this EventListener will handle the type of event.
		 * 
		 * @param Event $objEvent the triggered event.
		 * @return boolean true or false
		 */
		public function handlesEvent(Event $objEvent);
		
		
		/**
		 * This method implements the actual logic to handle the
		 * certain event.
		 * 
		 * @param Event $objEvent the triggered event.
		 */
		public function handleEvent(Event $objEvent);
	}