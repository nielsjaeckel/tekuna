<?php

	namespace org\tekuna\core\event;

	use org\tekuna\core\configuration\ConfigurationElement;
	use org\tekuna\core\context\Context;

	
	/**
	 * This abstract class implements some of the mandatory methods
	 * of the interfaces EventListener and Configured with standard 
	 * boiler plate logic.
	 */
	abstract class AbstractEventListener implements EventListener {
		
		private
			$objContext = NULL,
			$objConfigurationElement = NULL;
			
			
		public function setApplicationContext(Context $objContext) {
			
			$this -> objContext = $objContext;
		}
		
		public function getApplicationContext() {
			
			return $this -> objContext;
		}
		
		public function setConfigurationElement(ConfigurationElement $objElement) {
			
			$this -> objConfigurationElement = $objElement;
		}
		
		public function getConfigurationElement() {
			
			return $this -> objConfigurationElement;
		}
	}
