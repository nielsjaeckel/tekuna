<?php
	
	namespace org\tekuna\core\event;
	
	/**
	 * This event is triggered when the Tekuna-based application
	 * may start to run.
	 */
	class ApplicationRunEvent implements Event {
	
	}