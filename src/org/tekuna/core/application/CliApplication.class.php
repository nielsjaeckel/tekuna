<?php

	namespace org\tekuna\core\application;


	/**
	 * Class representing an application that was invoked via the command line interface (CLI).
	 */
	class CliApplication extends Application {

	}
