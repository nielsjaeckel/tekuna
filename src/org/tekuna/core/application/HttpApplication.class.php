<?php

	namespace org\tekuna\core\application;


	/**
	 * Class representing an application that was invoked via a HTTP(S) request.
	 */
	class HttpApplication extends Application {

	}
