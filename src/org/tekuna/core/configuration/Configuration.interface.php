<?php

	namespace org\tekuna\core\configuration;

	use org\tekuna\core\context\Context;


	/**
	 * Interface for a configuration that is used to configure an application.
	 * The application builder method gets an instance of this interface.
	 */
	interface Configuration  {


		/**
		 * This method triggers the actual loading of the configuration. For
		 * example this could load some configuration files or parse annotations.
		 *
		 * @param Context $objContext the context to load the configuration with
		 */
		public function loadConfiguration(Context $objContext);


		/**
		 * This method returns the root element (of type ConfigurationElement) of
		 * the loaded configuration.
		 *
		 * @return ConfigurationElement the root element of the configuration tree
		 */
		public function getRootElement();
	}