<?php

	namespace org\tekuna\core\configuration;

	use \Exception;


	/**
	 * This exception is thrown by the classes implementing the Configuration interface.
	 * Any kind of error regarding the configuration (syntactically or semantically) should be
	 * reported as ConfigurationException (that may contain previous exceptions).
	 */
	class ConfigurationException extends Exception {
	
	}
