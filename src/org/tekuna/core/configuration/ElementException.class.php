<?php

	namespace org\tekuna\core\configuration;


	/**
	 * This special ConfigurationException is thrown by the ConfigurationElement, if a certain operation
	 * with ConfigurationElements results in an error.
	 */
	class ElementException extends ConfigurationException {

	}
