<?php

	namespace org\tekuna\core\configuration;

	use org\tekuna\core\context\Context;


	/**
	 * Interface that represents objects with a direct representation
	 * inside the Configuration.
	 * 
	 * Examples: EventListener, Action, Interceptor
	 */
	interface Configured  {

		/**
		 * Set the ConfigurationElement of this instance
		 * 
		 * @param ConfigurationElement $objElement
		 */
		public function setConfigurationElement(ConfigurationElement $objElement);
		
		
		/**
		 * @return ConfigurationElement the configuration element that 
		 *         is associated to this instance
		 */
		public function getConfigurationElement();
	}