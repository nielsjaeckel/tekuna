<?php

	namespace org\tekuna\core\configuration;


	/**
	 * This special ConfigurationException is thrown by the ConfigurationElement or ConfigurationAttribute, if a certain operation
	 * with ConfigurationAttributes results in an error.
	 */
	class NoSuchAttributeException extends AttributeException {

	}
