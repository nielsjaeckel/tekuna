<?php

	namespace org\tekuna\core\configuration;


	/**
	 * This special ConfigurationException is thrown by the ConfigurationElement, if a certain operation
	 * on its children or parent is not possible.
	 */
	class NoSuchElementException extends ElementException {

	}
