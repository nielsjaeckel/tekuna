<?php

	namespace org\tekuna\core\configuration;


	/**
	 * The attribute of a ConfigurationElement. This class contains
	 * methods to manage the parent relation to the ConfigurationElement
	 * and it provides a nice __toString() implementation.
	 */
	class ConfigurationAttribute extends ConfigurationNode {

		/** the parent element */
		private $objElement = NULL;


		/**
		 * Set the parent element. If there is already another parent element
		 * (that means the attribute is assigned to a new element) the attrubute
		 * (this object) removes itself from the old parent node and stores the
		 * new parent node.
		 *
		 * @param ConfigurationElement $objElement the new parent element.
		 * @param boolean $bDisconnectParent calls removeAttribute on the old element if set to true
		 * @param boolean $bPopulateChild calls addAttribute on the new element if set to true
		 */
		public function setElement(ConfigurationElement $objElement, $bDisconnectParent = true, $bPopulateChild = true) {

			// disconnect from old element if present
			if ($bDisconnectParent && $this -> objElement !== NULL) {

				$this -> objElement -> removeAttribute($this, false);
			}

			// add this attribute to the new element
			if ($bPopulateChild && $objElement !== null) {

				$objElement -> addAttribute($this, false);
			}

			$this -> objElement = $objElement;
		}


		/**
		 * Returns the parent configuration node of type ConfigurationElement.
		 *
		 * @return the parent element object
		 */
		public function getElement() {

			return $this -> objElement;
		}


		/**
		 * This method returns a string representation of this attribute
		 * of the form
		 * aspect:name=value
		 *
		 * @return the representation of this attribute
		 */
		public function __toString() {

			return "$this->sAspect:$this->sName=$this->sValue";
		}
	}
