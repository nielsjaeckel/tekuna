<?php

	namespace org\tekuna\core\configuration;
	
	
	/**
	 * Abstract implementation of a Configuration. This class provides some
	 * generic getter and setter methods and can be used to implement the 
	 * Configuration interface.
	 */
	abstract class AbstractConfiguration implements Configuration {

		/** This is where the root element is stored */
		protected $objRootElement = NULL;

		
		/**
		 * Set the root configuration element of type ConfigurationElement.
		 * 
		 * @param ConfigurationElement $objRootElement the root element object
		 */
		public function setRootElement(ConfigurationElement $objRootElement) {
			
			$this -> objRootElement = $objRootElement;
		}

		
		/**
		 * This method returns the root element (of type ConfigurationElement) of 
		 * the loaded configuration.
		 * 
		 * @return ConfigurationElement the root element of the configuration tree
		 */
		public function getRootElement() {
		
			return $this -> objRootElement;
		}
		
		
		/**
		 * This method provides a nice string representation of this configuration. 
		 * The class of this object and the (nicely formatted) configuration node tree
		 * is returned.
		 * 
		 * @return string string representation
		 */
		public function __toString() {
			
			$sOut = get_class($this) .": \n";
			
			if ($this -> objRootElement == null) {
				
				$sOut .= '[null]';
			}
			else {
			
				$sOut .= $this -> objRootElement -> __toString();
			}
			
			return $sOut;
		}
	}