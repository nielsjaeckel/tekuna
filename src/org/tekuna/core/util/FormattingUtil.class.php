<?php

	namespace org\tekuna\core\util;
	
	
	/**
	 * Utility class that contains various helper methods for different
	 * formatting purposes.
	 */
	class FormattingUtil {

		/**
		 * This method returns a human readable representation of the
		 * given variable. Special handling of:
		 * 
		 *   * objects
		 *   * arrays
		 *   * empty strings
		 *   * null, true, false
		 * 
		 * @param mixed $mVar
		 */
		public static function getReadable($mVar) {
			
			// show strings with special handling of empty strings
			if (is_string($mVar)) {
				
				if ($mVar === '') {
					
					return "[empty string]";
				}
				else {
					
					return $mVar;
				}
			}
			
			// show the object type
			if (is_object($mVar)) {
				
				return '[object of type '. get_class($mVar) .']';
			}
			
			// show the count of arrays
			if (is_array($mVar)) {
				
				if ($mVar === array()) {
					
					return '[empty array]';
				}
				elseif (count($mVar) == 1) {
					
					return '[array with one element]';
				}
				else {
					
					return '[array with '. count($mVar) .' elements]';
				}
			}
			
			// show TRUE or FALSE
			if (is_bool($mVar)) {
				
				if ($mVar) {
					
					return '[TRUE]';
				}
				else {
					
					return '[FALSE]';
				}
			}
			
			// show NULL
			if ($mVar === null) {
				
				return '[NULL]';
			}
			
			// show floats, integers and resources directly
			if (is_float($mVar) || is_integer($mVar) || is_resource($mVar)) {
				
				return (string) $mVar;
			}
			
			// fallback. hope it is never used.
			return '[unknown]';
		}
	}
	