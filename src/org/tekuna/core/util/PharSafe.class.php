<?php

	namespace org\tekuna\core\util;
	
	
	/**
	 * Helper class with php functions made PHAR safe.
	 */
	class PharSafe {
		
		/**
		 * Because of Bug #52769 we need an own wrapper for the
		 * realpath function that is able to handle PHARs as well.
		 * 
		 * http://bugs.php.net/bug.php?id=52769
		 * 
		 * 
		 * @param unknown_type $sPath
		 */
		public static function realpath($sPath) {
			
			if (stripos($sPath, 'phar://') !== false) {
				
				// slice the path
				$iPharPos = stripos($sPath, '.phar');
				$sPharPath = substr($sPath, 7, $iPharPos + 5 - 7);
				$sPathInPhar = substr($sPath, $iPharPos + 5);
				
				// make path to phar absolute
				$sPharPath = realpath($sPharPath);
				
				// return new path with absolute phar path
				return 'phar://'. $sPharPath . $sPathInPhar;
			}
			else {
			
				return realpath($sPath);
			}
		}
	}
