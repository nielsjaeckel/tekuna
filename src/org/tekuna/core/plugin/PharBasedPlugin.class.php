<?php

	namespace org\tekuna\core\plugin;
	
	
	use org\tekuna\base\classloader\PharClassLoader;

	class PharBasedPlugin extends Plugin {
		
		protected $sPluginPhar;
		
		
		public function __construct($sPluginPhar) {
			
			if (! is_file($sPluginPhar)) {
				
				throw new PluginException("The PHP archive '$sPluginPhar' does not exist.");
			}
			
			$this -> sPluginPhar = $sPluginPhar;
			$this -> loadPluginConfiguration('phar://'. $sPluginPhar .'/'. self :: PLUGIN_CONFIGURATION_FILE);
		}
		
		
		function getResourceFilename($sResourcePath) {
			
			return 'phar://'. $this -> sPluginPhar .'/'. $sResourcePath;
		}
		
		
		public function getPluginClassLoader() {
			
			return new PharClassLoader($this -> sPluginPhar);
		}
		
		
		public function __toString() {
			
			// TODO
		}
	}
