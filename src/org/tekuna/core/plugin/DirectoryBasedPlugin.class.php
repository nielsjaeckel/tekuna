<?php

	namespace org\tekuna\core\plugin;
	
	use org\tekuna\base\classloader\DirectoryClassLoader;
	
	
	class DirectoryBasedPlugin extends Plugin {
		
		protected $sPluginDir;
		
		
		public function __construct($sPluginDir) {
			
			if (! is_dir($sPluginDir)) {
				
				throw new PluginException("The plugin directory '$sPluginDir' does not exist.");
			}
			
			$this -> sPluginDir = $sPluginDir;
			$this -> loadPluginConfiguration($sPluginDir .'/'. self :: PLUGIN_CONFIGURATION_FILE);
		}
		
		
		function getResourceFilename($sResourcePath) {
			
			return $this -> sPluginDir .'/'. $sResourcePath;
		}
		
		
		public function getPluginClassLoader() {
			
			return new DirectoryClassLoader($this -> sPluginDir .'/src');
		}
		
		
		public function __toString() {
			
			// TODO
		}
	}
