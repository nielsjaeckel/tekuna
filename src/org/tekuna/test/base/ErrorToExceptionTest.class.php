<?php

	namespace org\tekuna\test\base;

	
	use \PHPUnit_Framework_TestCase;

	class ErrorToExceptionTest extends PHPUnit_Framework_TestCase {


		/**
		 * @expectedException org\tekuna\base\exception\PHPWarningException
		 */
		public function testWarning() {

			strpos();
		}
		
		/**
		 * @expectedException org\tekuna\base\exception\PHPNoticeException
		 */
		public function testNotice() {

			$a = B;
		}
		
		
		public function testSuppressNotice() {
		
			@$a = B;	
		}
		
		
		public function testErrorLevel() {
		
			// deactivate notices
			error_reporting(E_ALL && !E_NOTICE);

			$a = B;	
		}
	}
