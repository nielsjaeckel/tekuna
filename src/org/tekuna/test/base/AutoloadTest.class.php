<?php
	
	namespace org\tekuna\test\base;


	use \PHPUnit_Framework_TestCase;

	class AutoloadTest extends PHPUnit_Framework_TestCase {


		public function testLoadingClass() {

			// test for class existence without triggering autoloading
			$this -> assertFalse(class_exists('org\tekuna\test\base\ClassToTestAutoloading', false));

			// now test for class existence with triggering autoloading
			$this -> assertTrue(class_exists('org\tekuna\test\base\ClassToTestAutoloading', true));
			
			// this should no more trigger any error
			new ClassToTestAutoloading();
		}


		public function testLoadingInterface() {

			// test for class existence without triggering autoloading
			$this -> assertFalse(interface_exists('org\tekuna\test\base\InterfaceToTestAutoloading', false));

			// now test for class existence with triggering autoloading
			$this -> assertTrue(interface_exists('org\tekuna\test\base\InterfaceToTestAutoloading', true));
		}
	}
