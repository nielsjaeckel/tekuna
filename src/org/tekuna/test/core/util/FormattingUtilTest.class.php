<?php

	namespace org\tekuna\test\core;

	
	use \PHPUnit_Framework_TestCase;
	use org\tekuna\core\util\FormattingUtil;

	class FormattingUtilTest extends PHPUnit_Framework_TestCase {
		
		public function testReadableVars() {
			
			$this -> assertEquals('[empty string]', FormattingUtil :: getReadable(''));
			$this -> assertEquals('testString', FormattingUtil :: getReadable('testString'));
			$this -> assertEquals('[object of type org\tekuna\test\core\FormattingUtilTest]', FormattingUtil :: getReadable($this));
			$this -> assertEquals('[empty array]', FormattingUtil :: getReadable(array()));
			$this -> assertEquals('[array with one element]', FormattingUtil :: getReadable(array(1)));
			$this -> assertEquals('[array with one element]', FormattingUtil :: getReadable(array($this)));
			$this -> assertEquals('[array with 3 elements]', FormattingUtil :: getReadable(array(1, 2, 3)));
			$this -> assertEquals('[TRUE]', FormattingUtil :: getReadable(true));
			$this -> assertEquals('[FALSE]', FormattingUtil :: getReadable(false));
			$this -> assertEquals('[NULL]', FormattingUtil :: getReadable(null));
			$this -> assertEquals('42', FormattingUtil :: getReadable(42));
			$this -> assertEquals('8.15', FormattingUtil :: getReadable(8.15));
			$this -> assertEquals('0', FormattingUtil :: getReadable(0));
			
			$hFile = fopen(__FILE__, 'r');
			$this -> assertEquals((string) $hFile, FormattingUtil :: getReadable($hFile));
			fclose($hFile);
		}
	}
