<?php

	namespace org\tekuna\test\core;
	
	use \PHPUnit_Framework_TestCase;
	use org\tekuna\core\util\WeightedList;

	
	class WeightedListTest extends PHPUnit_Framework_TestCase {
		
		public function testFloatWeight() {
			
			$objTextList = new WeightedList();
			$objTextList -> addElement('w-2a', 2);
			$objTextList -> addElement('w-1', '1');
			$objTextList -> addElement('w-3', 3);
			$objTextList -> addElement('w-2b', 2);
			$objTextList -> addElement('w-2.5', 2.5);
			
			$this -> assertEquals('w-1,w-2a,w-2b,w-2.5,w-3', implode(',', $objTextList -> getSortedList()));
		}

		public function testMinWeight() {
			
			$objTextList = new WeightedList();
			$objTextList -> addElement('w-2', 2);
			$objTextList -> addElement('w--1', -1);
			$objTextList -> addElement('w-min1', 'min');
			$objTextList -> addElement('w-3', 3);
			$objTextList -> addElement('w-2.5', 2.5);
			$objTextList -> addElement('w-min2', 'min');
			
			$this -> assertEquals('w-min1,w-min2,w--1,w-2,w-2.5,w-3', implode(',', $objTextList -> getSortedList()));
		}
	
		public function testMaxWeight() {
			
			$objTextList = new WeightedList();
			$objTextList -> addElement('w-2', 2);
			$objTextList -> addElement('w-max1', 'max');
			$objTextList -> addElement('w-1', 1);
			$objTextList -> addElement('w-max2', 'max');
			$objTextList -> addElement('w-3', 3);
			$objTextList -> addElement('w-2.5', 2.5);
			
			$this -> assertEquals('w-1,w-2,w-2.5,w-3,w-max1,w-max2', implode(',', $objTextList -> getSortedList()));
		}
	}
