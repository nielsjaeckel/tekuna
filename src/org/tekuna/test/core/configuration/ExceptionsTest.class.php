<?php

	namespace org\tekuna\test\core\configuration;


	use \PHPUnit_Framework_TestCase;

	use org\tekuna\core\configuration\ConfigurationNode;
	use org\tekuna\core\configuration\ConfigurationElement;
	use org\tekuna\core\configuration\ConfigurationAttribute;


	class ExceptionsTest extends PHPUnit_Framework_TestCase {

		private
			$objRootElement,
			$objChildElement1,
			$objChildElement2,
			$objChildElement3,
			$objAttribute1,
			$objAttribute2;


		public function setUp() {

			$this -> objRootElement = new ConfigurationElement('test', 'root', 'root');
			$this -> objChildElement1 = new ConfigurationElement('test', 'type', 'child1');
			$this -> objChildElement2 = new ConfigurationElement('test', 'class', 'child2');
			$this -> objChildElement3 = new ConfigurationElement('live', 'class', 'child3');

			$this -> objAttribute1 = new ConfigurationAttribute('test', 'name', 'attr1');
			$this -> objAttribute2 = new ConfigurationAttribute('test', 'name2', 'attr2');

			$this -> objRootElement -> addChildElement($this -> objChildElement1);
			$this -> objRootElement -> addChildElement($this -> objChildElement2);
			$this -> objChildElement2 -> addChildElement($this -> objChildElement3);

			$this -> objRootElement -> addAttribute($this -> objAttribute1);
			$this -> objRootElement -> addAttribute($this -> objAttribute2);
		}

		/**
		 * @expectedException org\tekuna\core\configuration\ElementException
		 */
		public function testAddChildAgain() {

			$this -> objRootElement -> addChildElement($this -> objChildElement1);
		}

		/**
		 * @expectedException org\tekuna\core\configuration\NoSuchElementException
		 */
		public function testRemoveUnknownChild() {

			$this -> objRootElement -> removeChildElement($this -> objChildElement3);
		}

		/**
		 * @expectedException org\tekuna\core\configuration\NoSuchElementException
		 */
		public function testNoFirstChildElement() {

			$this -> objRootElement -> getFirstChildElement('unknownAspect', 'unknownName');
		}

		/**
		 * @expectedException org\tekuna\core\configuration\AttributeException
		 */
		public function testAddAttributeAgain() {

			$this -> objRootElement -> addAttribute($this -> objAttribute1);
		}

		/**
		 * @expectedException org\tekuna\core\configuration\NoSuchAttributeException
		 */
		public function testRemoveUnknownAttribute() {

			$this -> objChildElement1 -> removeAttribute($this -> objAttribute1);
		}

		/**
		 * @expectedException org\tekuna\core\configuration\NoSuchAttributeException
		 */
		public function testNoSuchAttribute() {

			$this -> objChildElement1 -> getAttribute('unknownAspect', 'unknownName');
		}
	}
