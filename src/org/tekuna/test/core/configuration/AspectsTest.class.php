<?php

	namespace org\tekuna\test\core\configuration;


	use \PHPUnit_Framework_TestCase;

	use org\tekuna\core\configuration\ConfigurationNode;
	use org\tekuna\core\configuration\ConfigurationElement;
	use org\tekuna\core\configuration\ConfigurationAttribute;


	class AspectsTest extends PHPUnit_Framework_TestCase {

		private
			$objRootElement,
			$objChildElement1,
			$objChildElement2,
			$objChildElement3,
			$objAttribute1,
			$objAttribute2;


		public function setUp() {

			$this -> objRootElement = new ConfigurationElement('test', 'root', 'root');
			$this -> objChildElement1 = new ConfigurationElement('test', 'type', 'child1');
			$this -> objChildElement2 = new ConfigurationElement('test', 'class', 'child2');
			$this -> objChildElement3 = new ConfigurationElement('test', 'class', 'child3');

			$this -> objAttribute1 = new ConfigurationAttribute('test', 'name', 'attr1');
			$this -> objAttribute2 = new ConfigurationAttribute('test', 'name2', 'attr2');

			$this -> objRootElement -> addChildElement($this -> objChildElement1);
			$this -> objRootElement -> addChildElement($this -> objChildElement2);
			$this -> objChildElement2 -> addChildElement($this -> objChildElement3);

			$this -> objRootElement -> addAttribute($this -> objAttribute1);
			$this -> objRootElement -> addAttribute($this -> objAttribute2);
		}


		public function testChangeAspectCompletely() {

			// change aspect on root element
			$this -> objRootElement -> changeAspect('test', 'test2');

			// all aspects changed (elements, attributes)
			$this -> assertEquals('test2', $this -> objRootElement -> getAspect());
			$this -> assertEquals('test2', $this -> objChildElement1 -> getAspect());
			$this -> assertEquals('test2', $this -> objChildElement2 -> getAspect());
			$this -> assertEquals('test2', $this -> objChildElement3 -> getAspect());
			$this -> assertEquals('test2', $this -> objAttribute1 -> getAspect());
			$this -> assertEquals('test2', $this -> objAttribute2 -> getAspect());
		}


		public function testChangeAspectPartially() {

			$this -> objChildElement2 -> changeAspect('test', 'test2');

			// childelements changed
			$this -> assertEquals('test2', $this -> objChildElement2 -> getAspect());
			$this -> assertEquals('test2', $this -> objChildElement3 -> getAspect());

			// parent element and siblings and attributes of parent untouched
			$this -> assertEquals('test', $this -> objRootElement -> getAspect());
			$this -> assertEquals('test', $this -> objChildElement1 -> getAspect());
			$this -> assertEquals('test', $this -> objAttribute1 -> getAspect());
			$this -> assertEquals('test', $this -> objAttribute2 -> getAspect());
		}
	}
