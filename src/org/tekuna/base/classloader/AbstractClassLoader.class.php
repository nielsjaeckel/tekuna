<?php

	namespace org\tekuna\base\classloader;


	/**
	 * Abstract ClassLoader implementation that does not implement the actual class loading logic
	 * but provides some convenience methods that ease the work with classes and packages.
	 */
	abstract class AbstractClassLoader implements ClassLoader {
	
		/**
		 * Extracts the namespace part of a fully qualified class name.
		 * 
		 * @param String $sFullQualifiedClassName the fully qualified class name
		 * @return String the extracted namespace
		 */
		public static function extractNamespace($sFullQualifiedClassName) {
			
			return substr($sFullQualifiedClassName, 0, strrpos($sFullQualifiedClassName, '\\'));
		}


		/**
		 * Extracts the class name part of a fully qualified class name.
		 * 
		 * @param String $sFullQualifiedClassName the fully qualified class name
		 * @return String the extracted class name
		 */
		public static function extractClassName($sFullQualifiedClassName) {
			
			return substr($sFullQualifiedClassName, strrpos($sFullQualifiedClassName, '\\') + 1);
		}
	}
