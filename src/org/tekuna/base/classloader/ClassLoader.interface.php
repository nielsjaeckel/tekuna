<?php

	namespace org\tekuna\base\classloader;

	/** A convenience constant for the classloader package that is just an alias for DIRECTORY_SEPARATOR. */
	const DS = DIRECTORY_SEPARATOR;


	/**
	 * The ClassLoader interface is used by the Tekuna main class when registering
	 * and invoking automated class loads. Implement this interface to provide your 
	 * own ClassLoader.
	 */
	interface ClassLoader {
	
		/**
		 * This method must implement the actual class loading logic. 
		 * 
		 * @param String $sNamespace the name space in which the class is expected
		 * @param String $sClassName the name of the class without namespace prefix
		 */
		public function loadClass($sNamespace, $sClassName);
		
		
		/**
		 * This method must provide the class name and important parameters of the class
		 * loader (e.g. the base directory for directory-based class loading). It is used
		 * by the Tekuna main class for logging.
		 * 
		 * @return String concise representation of the ClassLoader
		 */
		public function __toString();
	}
