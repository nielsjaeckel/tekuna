<?php

	namespace org\tekuna\base\classloader;
	
	use \Exception;


	/**
	 * This exception may be used by concrete ClassLoader implementations for 
	 * reporting exceptional cases.
	 */
	class ClassLoadException extends Exception {
	
	}
