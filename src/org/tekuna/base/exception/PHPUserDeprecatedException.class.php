<?php

	namespace org\tekuna\base\exception;


	/**
	 * Exception that wraps an E_USER_DEPRECATED php error.
	 */

	class PHPUserDeprecatedException extends AbstractPHPErrorException {


	}
