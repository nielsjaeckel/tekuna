<?php

	namespace org\tekuna\base\exception;


	/**
	 * Exception that wraps an E_NOTICE php error.
	 */

	class PHPNoticeException extends AbstractPHPErrorException {


	}
