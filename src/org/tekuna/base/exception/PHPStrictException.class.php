<?php

	namespace org\tekuna\base\exception;


	/**
	 * Exception that wraps an E_STRICT php error.
	 */

	class PHPStrictException extends AbstractPHPErrorException {


	}
