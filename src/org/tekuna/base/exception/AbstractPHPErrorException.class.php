<?php

	namespace org\tekuna\base\exception;

	use \Exception;


	/**
	 * Parent class for all other Exceptions that wrap php errors. It is just
	 * used to make it possible to catch all php-error related Exceptions at once.
	 */
	class AbstractPHPErrorException extends Exception {
	
	}
