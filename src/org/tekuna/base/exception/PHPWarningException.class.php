<?php

	namespace org\tekuna\base\exception;


	/**
	 * Exception that wraps an E_WARNING php error.
	 */

	class PHPWarningException extends AbstractPHPErrorException {


	}
