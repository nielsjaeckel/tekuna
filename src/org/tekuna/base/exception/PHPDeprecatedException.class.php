<?php

	namespace org\tekuna\base\exception;


	/**
	 * Exception that wraps an E_DEPRECATED php error.
	 */

	class PHPDeprecatedException extends AbstractPHPErrorException {


	}
