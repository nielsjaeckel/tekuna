<?php

	namespace org\tekuna\base\exception;


	/**
	 * Exception that wraps an E_USER_ERROR php error.
	 */

	class PHPUserErrorException extends AbstractPHPErrorException {


	}
