<?php

	namespace org\tekuna\base\exception;


	/**
	 * Exception that wraps an E_USER_WARNING php error.
	 */

	class PHPUserWarningException extends AbstractPHPErrorException {


	}
