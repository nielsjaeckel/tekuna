<?php

	namespace org\tekuna\base\exception;


	/**
	 * Exception that wraps an E_RECOVERABLE_ERROR php error.
	 */

	class PHPRecoverableErrorException extends AbstractPHPErrorException {


	}
