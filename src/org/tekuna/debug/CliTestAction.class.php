<?php

	namespace org\tekuna\debug;

	use org\tekuna\core\context\Context;
	
	use org\tekuna\framework\interceptor\Interceptor;
	use org\tekuna\framework\interceptor\InvocationChain;
	use org\tekuna\framework\action\AbstractAction;
	use org\tekuna\framework\action\ActionEvent;
	use org\tekuna\framework\request\Request;
	use org\tekuna\framework\response\http\ContentResponse;
	use org\tekuna\framework\response\http\RedirectResponse;
	use org\tekuna\framework\response\http\FileResponse;
	
	
	class CliTestAction extends AbstractAction implements Interceptor {

		public function execute(ActionEvent $objActionEvent, Request $objRequest) {

			$sOutput = 'action perform'."\n";
			$sOutput .= 'configuration element: '. $this -> getConfigurationElement() ."\n";
			
			echo $sOutput;
			
			#throw new \Exception("jaja und so.", -1, new \Exception("die ursache."));
		}

		public function intercept(InvocationChain $objChain) {

			static $iNestingLevel = 0;
			$iMyLevel = ++$iNestingLevel;
			
			$sSpecialName = $this -> getConfigurationElement() -> getAttribute('framework', 'specialName') -> getValue();

			echo 'pre intercept '. $iMyLevel .' ['. $sSpecialName .']'."\n";
			$objResponse = $objChain -> invokeNext();
			echo 'post intercept '. $iMyLevel .' ['. $sSpecialName .']'."\n";
			
			return $objResponse;
		}

		public function setRequest($objRequest) {

			echo 'injected request: '. get_class($objRequest) ."\n";
		}

		public function setAppTitle($sAppTitle) {

			echo 'injected app title: '. $sAppTitle ."\n";
		}
		
		public function setConfiguration($objConfiguration) {

			echo 'injected config: '. get_class($objConfiguration) ."\n";
		}
	}