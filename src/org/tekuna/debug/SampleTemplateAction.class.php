<?php

	namespace org\tekuna\debug;
	
	use org\tekuna\plugin\simpletemplate\SimpleTemplateAction;
	use org\tekuna\framework\action\ActionEvent;
	use org\tekuna\framework\request\Request;
	
	
	class SampleTemplateAction extends SimpleTemplateAction {
		
		protected
			$name;
		
		function executeTemplate(ActionEvent $objActionEvent, Request $objRequest) {
			
			$this -> name = 'blubberblase';
		}
	}
