<?php

	namespace org\tekuna\debug;

	use org\tekuna\framework\response\http\FileDownloadResponse;

	use org\tekuna\core\context\Context;
	
	use org\tekuna\framework\interceptor\Interceptor;
	use org\tekuna\framework\interceptor\InvocationChain;
	use org\tekuna\framework\action\AbstractAction;
	use org\tekuna\framework\action\ActionEvent;
	use org\tekuna\framework\request\Request;
	use org\tekuna\framework\response\http\ContentResponse;
	use org\tekuna\framework\response\http\RedirectResponse;
	use org\tekuna\framework\response\http\FileResponse;
	
	
	class TestAction extends AbstractAction implements Interceptor {

		public function execute(ActionEvent $objActionEvent, Request $objRequest) {
		#	echo "asdf";
		#	return "asdf";
			
			#return new FileResponse(__FILE__);
			#return new RedirectResponse('http://www.nielsjaeckel.de');
			
			$sOutput = 'action perform<br><pre>';
			#$sOutput .= 'configuration element: <pre>'. $this -> getConfigurationElement() .'</pre>';
			#$sOutput .= '<form method="post" action="#" enctype="multipart/form-data"><input type="file" name="myUploadedFile" /><br><input type="text" name="testparam" /><input type="submit" name="submitButton" value="Submit!" /></form>';
			
			$objPCM = $this -> getApplicationContext() -> getPdoConnectionManager();
			$objConn = $objPCM -> getDefaultConnection();
			
			#$this -> blubb();
			
			foreach ($objConn -> query('select * from users;') as $arrRow) {

				$sOutput .= print_r($arrRow, true);
			}
			
			$sOutput .= '</pre>';
			
			$sOutput .= $objRequest -> buildCurrentRequestUrl() -> renderAbsolute();
			
			#print_r();
			
			#return new FileDownloadResponse(__FILE__);
			
			return new ContentResponse($sOutput);
		}

		public function intercept(InvocationChain $objChain) {

			static $iNestingLevel = 0;
			$iMyLevel = ++$iNestingLevel;
			
			$sSpecialName = $this -> getConfigurationElement() -> getAttribute('framework', 'specialName') -> getValue();

			#echo 'pre intercept '. $iMyLevel .' ['. $sSpecialName .']<br>';
			$objResponse = $objChain -> invokeNext();
			#echo 'post intercept '. $iMyLevel .' ['. $sSpecialName .']<br>';
			
			return $objResponse;
		}

		public function setRequest($objRequest) {

			#echo 'injected request: '. get_class($objRequest) .'<br>';
		}

		public function setAppTitle($sAppTitle) {

			#echo 'injected app title: '. $sAppTitle .'<br>';
		}
		
		public function setConfiguration($objConfiguration) {

			#echo 'injected config: '. get_class($objConfiguration) .'<br>';
		}
	}