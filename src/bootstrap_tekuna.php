<?php

	// load log4php
	require_once 'lib/log4php-2.0.0/Logger.php';

	// load tekuna
	require_once 'src/org/tekuna/base/Tekuna.class.php';
	require_once 'src/org/tekuna/base/classloader/ClassLoadException.class.php';
	require_once 'src/org/tekuna/base/classloader/ClassLoader.interface.php';
	require_once 'src/org/tekuna/base/classloader/AbstractClassLoader.class.php';
	require_once 'src/org/tekuna/base/classloader/DirectoryClassLoader.class.php';

	// start bootstrapping
	org\tekuna\base\Tekuna :: bootstrap(getcwd());

