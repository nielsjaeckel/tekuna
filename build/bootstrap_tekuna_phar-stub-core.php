<?php

	// require the absolutely necessary files manually
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/Tekuna.class.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/ClassLoadException.class.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/ClassLoader.interface.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/AbstractClassLoader.class.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/DirectoryClassLoader.class.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/PharClassLoader.class.php';


	use org\tekuna\base\Tekuna;
	use org\tekuna\base\classloader\PharClassLoader;
	use org\tekuna\core\application\Application;
	use org\tekuna\core\configuration\XmlConfiguration;


	// and then bootstrap the framework
	Tekuna :: bootstrap(getcwd());


	// add a class loader for this PHAR
	Tekuna :: getInstance() -> addClassLoader(new PharClassLoader(__FILE__));


	// load and run the application
	Application :: buildApplication(
		new XmlConfiguration('application.xml', 'http://namespaces.tekuna.org/core/##TEKUNA_VERSION##/')
	) -> run();


	__HALT_COMPILER();