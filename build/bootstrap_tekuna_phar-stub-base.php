<?php

	// require the absolutely necessary files manually
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/Tekuna.class.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/ClassLoadException.class.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/ClassLoader.interface.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/AbstractClassLoader.class.php';
	require_once 'phar://'. __FILE__ .'/src/org/tekuna/base/classloader/DirectoryClassLoader.class.php';

	// and then bootstrap the framework
	org\tekuna\base\Tekuna :: bootstrap(getcwd());

	__HALT_COMPILER();