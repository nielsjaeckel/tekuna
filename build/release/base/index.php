<?php

	// load log4php
	require_once 'lib/log4php-2.0.0.phar';

	// Load Tekuna from the PHAR inside the lib/ directory.
	// The initial setup of the ClassLoader is done while this require.
	require_once 'lib/tekuna-base-##TEKUNA_VERSION##.phar';

	// apply class loading and print the obligatory message
	org\tekuna\demo\base\HelloWorld :: greet();
