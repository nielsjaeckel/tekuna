<?php
	
	namespace org\tekuna\demo\core;

	use org\tekuna\core\event\Event;
	use org\tekuna\core\event\AbstractEventListener;
	use org\tekuna\core\event\ApplicationRunEvent;
	use org\tekuna\core\context\Context;
	
	
	class ContextListener extends AbstractEventListener {
		
		public function __construct() {
			
			echo '<h1>The Tekuna core works.</h1>';
			echo '<p>Hello World!</p>';
		}
		
		public function handlesEvent(Event $objEvent) {
			
			return $objEvent instanceof ApplicationRunEvent;
		}
		
		public function handleEvent(Event $objEvent) {
			
			// get the config from the context
			$objConfig = $this -> getApplicationContext() -> getConfiguration();
			
			// load all entry nodes of the defined aspect
			$arrEntries = $objConfig -> getRootElement() -> getAllChildElements('context', 'entry');
			
			// iterate all found elements
			foreach ($arrEntries as $objEntryElement) {
				
				// store the value in the context and use as name the content of the name attribute
				$sName = $objEntryElement -> getAttribute('context', 'name') -> getValue();
				$sValue = $objEntryElement -> getValue();
				
				$this -> getApplicationContext() -> setObject($sName, $sValue);
			}
		}
	}
