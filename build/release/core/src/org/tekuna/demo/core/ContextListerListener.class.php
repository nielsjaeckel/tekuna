<?php
	
	namespace org\tekuna\demo\core;

	use org\tekuna\core\event\Event;
	use org\tekuna\core\event\AbstractEventListener;
	use org\tekuna\core\event\ApplicationRunEvent;
	use org\tekuna\core\context\Context;
	
	
	class ContextListerListener extends AbstractEventListener {
		
		public function handlesEvent(Event $objEvent) {
			
			return $objEvent instanceof ApplicationRunEvent;
		}
		
		public function handleEvent(Event $objEvent) {
					
			// get the config from the context
			$objConfig = $this -> getApplicationContext() -> getConfiguration();
			
			// load all show nodes of the defined aspect
			$arrEntries = $objConfig -> getRootElement() -> getAllChildElements('lister', 'show');
			
			// iterate all nodes and take the things to show from the context
			foreach ($arrEntries as $objShowElement) {
				
				$sName = $objShowElement -> getAttribute('lister', 'name') -> getValue();
				
				echo "<h2>Content of '$sName' in the application context:</h2>";
				echo '<pre>'. $this -> getApplicationContext() -> getObject($sName) .'</pre>';
			}
		}
	}
