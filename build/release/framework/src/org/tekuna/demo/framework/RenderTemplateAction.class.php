<?php

	namespace org\tekuna\demo\framework;

	use org\tekuna\framework\request\Request;
	use org\tekuna\framework\action\ActionEvent;
	
	use org\tekuna\plugin\simpletemplate\SimpleTemplateAction;

	
	class RenderTemplateAction extends SimpleTemplateAction {

		protected
			$objActionEvent,
			$objRequest;
		
		
		public function executeTemplate(ActionEvent $objActionEvent, Request $objRequest) {
			
			$this -> objActionEvent = $objActionEvent;
			$this -> objRequest = $objRequest;
		}
	}
