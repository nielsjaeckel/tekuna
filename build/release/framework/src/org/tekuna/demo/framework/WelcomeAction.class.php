<?php

	namespace org\tekuna\demo\framework;

	use org\tekuna\framework\request\Request;
	use org\tekuna\framework\action\ActionEvent;
	
	use org\tekuna\plugin\simpletemplate\SimpleTemplateAction;

	
	class WelcomeAction extends SimpleTemplateAction {
		
		protected $sCurrentDateTime;
		
		public function executeTemplate(ActionEvent $objActionEvent, Request $objRequest) {
			
			$this -> sCurrentDateTime = date('r');
			
			$this -> getApplicationContext() -> setAppTitle('Welcome!');
		}
	}
