<?php

	// load log4php
	require_once 'lib/log4php-2.0.0.phar';

	// Load Tekuna from the PHAR inside the lib/ directory.
	// The initial setup and application start is done implicitly.
	require_once 'lib/tekuna-framework-##TEKUNA_VERSION##.phar';
