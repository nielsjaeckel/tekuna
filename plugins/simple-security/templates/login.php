<?php if ($this -> errorMessage != '') { ?>
<p class="simple-security-login-error">
	<?php echo $this -> errorMessage; ?>
</p>
<?php } ?>

<form method="post" action="<?php echo $this -> loginUrl; ?>" class="simple-security-login-form">
	<input type="text" name="login" class="login" /><br />
	<input type="password" name="password" class="password" /><br />
	<input type="submit" value="Login" class="submit" />
</form>