<p>
	You are not authorized to view the action 
	<tt><?php echo $this -> unauthorizedAction; ?></tt>.
</p>
<p>
	<?php if ($this -> authenticatedUser === null) { ?>
	Please <a href="<?php echo $this -> loginUrl; ?>">login</a> and try again.
	<?php } else { ?>
	Please <a href="<?php echo $this -> loginUrl; ?>">login</a> as authorized user.
	<?php } ?>
</p>
