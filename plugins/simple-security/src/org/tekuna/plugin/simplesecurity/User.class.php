<?php

	namespace org\tekuna\plugin\simplesecurity;
	
	
	class User {
		
		public
			$login,
			$encryptedPassword,
			$name,
			$roles;
			
		
		public function hasRole($sRole) {
			
			return in_array($sRole, $this -> roles);
		}
	}