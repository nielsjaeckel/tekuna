<?php

	namespace org\tekuna\plugin\simplesecurity\action;
	
	use org\tekuna\framework\action\AbstractAction;
	use org\tekuna\framework\action\ActionEvent;
	use org\tekuna\framework\request\Request;
	use org\tekuna\framework\response\http\UrlRedirectResponse;
	use org\tekuna\framework\filter\data\FrameworkActionFilter;

	use org\tekuna\plugin\simplesecurity\SecurityManager;
	
	
	class LogoutAction extends AbstractAction {
		
		protected $objSecurityManager;

			
		public function execute(ActionEvent $objActionEvent, Request $objRequest) {
		
			// perform the logout
			$this -> objSecurityManager -> logoutUser();
				
			// send redirect
			if ($objRequest -> hasGetValue('afterLogoutAction')) {
				
				// get the unauthorized action
				$objRequest -> setGetFilter('afterLogoutAction', new FrameworkActionFilter());
				
				// redirect to given after logout action
				$objRedirectUrl = $objActionEvent -> buildActionUrl($objRequest -> getGetValue('afterLogoutAction'));
				return new UrlRedirectResponse($objRedirectUrl);
			}
			else {
				
				// redirect to default after logout action
				$objRedirectUrl = $objActionEvent -> buildActionUrl($this -> objSecurityManager -> getAfterLogoutAction());
				return new UrlRedirectResponse($objRedirectUrl);
			}
		}
		
		
		public function setSimpleSecurityManager(SecurityManager $objSecurityManager) {
			
			$this -> objSecurityManager = $objSecurityManager;
		}
	}
