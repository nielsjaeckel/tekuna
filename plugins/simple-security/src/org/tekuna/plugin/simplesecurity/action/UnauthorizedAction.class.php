<?php

	namespace org\tekuna\plugin\simplesecurity\action;
	
	use org\tekuna\framework\filter\data\FrameworkActionFilter;
	use org\tekuna\framework\action\ActionEvent;
	use org\tekuna\framework\request\Request;
	
	use org\tekuna\plugin\simpletemplate\SimpleTemplateAction;
	use org\tekuna\plugin\simplesecurity\SecurityManager;

	
	class UnauthorizedAction extends SimpleTemplateAction {
		
		protected
			$objSecurityManager,
			
			$unauthorizedAction,
			$authenticatedUser,
			$loginUrl;
		
		public function executeTemplate(ActionEvent $objActionEvent, Request $objRequest) {
		
			// get the unauthorized action
			$objRequest -> setGetFilter('unauthorizedAction', new FrameworkActionFilter());
			
			// provide navigation links
			$this -> unauthorizedAction = $objRequest -> getGetValue('unauthorizedAction');
			$this -> authenticatedUser = $this -> objSecurityManager -> getAuthenticatedUser();
			$this -> loginUrl = $objActionEvent -> buildActionUrl($this -> objSecurityManager -> getLoginAction());
			$this -> loginUrl -> setParameter('unauthorizedAction', $this -> unauthorizedAction);
		}
		
		
		public function setSimpleSecurityManager(SecurityManager $objSecurityManager) {
			
			$this -> objSecurityManager = $objSecurityManager;
		}
	}