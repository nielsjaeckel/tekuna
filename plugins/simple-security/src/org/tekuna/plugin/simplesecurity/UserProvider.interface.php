<?php

	namespace org\tekuna\plugin\simplesecurity;
	
	use org\tekuna\core\context\Context;

	
	interface UserProvider {
		
		public function initialize(Context $objContext);
		
		public function getUser($sLogin);
		
		public function encryptPassword($sLogin, $sPassword);
	}
