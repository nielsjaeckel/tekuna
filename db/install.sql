create table users (
	id integer primary key,
	login char(255),
	password char(255)
);

create table articles (
	id integer primary key,
	id_user integer,
	title char(1024),
	text char(102400)
);

insert into users (login, password) values ('admin', 'top-secret');
insert into users (login, password) values ('manager', 'secret');
insert into users (login, password) values ('user', 'weak');


