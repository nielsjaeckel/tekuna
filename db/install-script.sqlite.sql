-- create tables
create table articles (
	id integer primary key,
	key text,
	creationDate integer,
	author text,
	title text,
	text text
);

create table comments (
	id integer primary key,
	articleId integer,
	creationDate integer,
	author text,
	text text
);

-- insert demo data
insert into articles (key, creationDate, author, title, text) values ('first-weblog-article', 1000000, 'john', 'First Weblog Article', 'Hello World!');
insert into articles (key, creationDate, author, title, text) values ('second-weblog-article', 2000000, 'john', 'Second Weblog Article', 'Hello Universe!');

insert into comments (articleId, creationDate, author, text) values (1, 2001000, 'Arthur Dent', 'I like the universe, too!');
insert into comments (articleId, creationDate, author, text) values (1, 2002000, 'Trillian', 'I know all the universe...');
