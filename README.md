# Tekuna is an application container and framework for PHP applications. It provides a stable basis and a generic infrastructure for your projects. Tekuna has a small footprint and will not pollute your codebase with unnecessary stuff. Tekuna partially provides for PHP what Servlets, XWork and Spring provide for Java. #

## Tekuna's Features ##
* Component-oriented Definition of the Application
* Intuitive Routing of requests to Actions
* Friendly URLs with URI Routing
* MVC Architecure
* PHP Error handling with Exceptions (and thus catchable)
* Separate Request and Response Objects
* Flexible Architecture with Events, Interceptors, Filters and the Application Context
* Integrated Plugin System
* Support for PHAR, clever class loading
* Strict Input Filtering
* Eliminating the Effects of Magic Quotes
* Full UTF-8 Support
* Support for multiple Template Engines

## Non-functional Features ##
* It's Open Source
* Provides a standardized Project Structure
* Improves Reusability
* Fully Unit-tested
* 100 % E_STRICT and E_DEPRECATED Compliant (Produces no Errors, Warnings or Notices)
* Using Features PHP 5.3 (Namespaces, PHAR)
* Combinable with many other Frameworks (e.g. other Templating, ORM, Unit-Testing)
* Object-Oriented Architecture
* Geared to Java Programming and Naming Style




# Documentation #

## General Information ##
### System Requirements ###
Tekuna needs PHP 5.3 to run. Reading PHARs must be enabled.

Apache Webserver recommendet. Out-of-the-box runnable with mod_rewrite

### License (MIT License) ###
The MIT License

Copyright (c) 2009-2011 Niels Jäckel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Getting Started with Tekuna ##
Overall Architecture
Directory Structure

## Basic Tekuna Functions (Tekuna Base) ##

The main feature of the Tekuna Base is the transformation of PHP errors into exceptions. This enhances each PHP error with a trace. So you can track down errors more easily. Furthermore you are able to catch these exceptions and handle them accordingly.

### Handle PHP Errors as Exceptions ###
With the translation of PHP errors into exceptions it is possible to do something like this:


```
#!php

try {

   // let's trigger a PHP Warning
   // (print_r() expects at least 1 parameter, 0 given)
   print_r();
}
catch (PHPWarningException $e) {
        
   // some sophisticated error handling
}
```

Every PHP error has a corresponding exception, e.g.:

E_WARNING to PHPWarningException
E_NOTICE to PHPNotceException
E_STRICT to PHPStrictException
E_USER_ERROR to PHPUserErrorException
Sometimes PHP core functions and some legacy code may use PHP errors to communicate error situations. In these cases it is possible to catch the corresponding exceptions and apply a proper exception handling that works consistently to the exception handling every reasonable application should have.

### Class Loading ###

## Application Configuration ##
Defining the Application, Modules, Components and Controllers
Using Path Patterns
Applying Special Path Patterns

## Using the Features of Tekuna ##
Built-In Template Engines
The Application Context
Writing Multilanguage Applications
Request Data Filtering
Using the Response Object