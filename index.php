<?php

	require_once 'src/bootstrap_tekuna.php';

	use org\tekuna\base\Tekuna;
	use org\tekuna\core\application\Application;
	use org\tekuna\core\configuration\XmlConfiguration;


	Application :: buildApplication(
		new XmlConfiguration('application.xml', 'http://namespaces.tekuna.org/core/0.3/')
	) -> run();

